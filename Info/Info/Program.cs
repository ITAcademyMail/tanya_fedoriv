﻿using System;


namespace Info
{
    class Program
    {
        static void Main(string[] args)
        {
            string f_name = "Tanya";
            string s_name = "Fedoriv";

            ShowName(f_name);
            ShowSurname(s_name);

            string info = "Базові знання мови С++";
            Showinfo(info);

            Console.ReadKey();
        }

        static void ShowName(string name)
        {
            Console.WriteLine("My name is " + name);
        }


        static void ShowSurname(string surname)
        {
            Console.WriteLine("My surname is " + surname);
        }

        static void Showinfo(string information)
        {
            Console.WriteLine("Вміння у галузі програмування: " + information);
        }
    }
}
